/*
 * Kai Windle Copyright (c) 17/08/17 07:18/$month/$year
 * ExModel.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
data class ExModel(val firstname: String, val lastname: String, val age: Int)

