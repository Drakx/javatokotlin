/*
 * Kai Windle Copyright (c) 17/08/17 07:58/$month/$year
 * ExRecylerViewAdapter.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExRecylerViewAdapter(val items: ExModel, val listener: (ExModel) -> Unit) : RecyclerView.Adapter<ExRecylerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items, listener)

    override fun getItemCount() = items.age

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ExModel, listener: (ExModel) -> Unit) = with(itemView) {

            // Note this is just an example and probably won't work
            val itemTitle = TextView(context)
            itemTitle.text = item.firstname
            setOnClickListener { listener(item) }
        }
    }
}

