/*
 * Kai Windle Copyright (c) 17/08/17 07:18/$month/$year
 * ExEnumStringAnnotation.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

import android.support.annotation.StringDef
import com.kaiwindle.kotlinexamples.java.ExEnumStringAnnotation.SOME_OTHER_STRING
import com.kaiwindle.kotlinexamples.java.ExEnumStringAnnotation.SOME_STRING

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExEnumStringAnnotation {
    val SOME_STRING = "SOME STRING"
    val SOME_OTHER_STRING = "SOME OTHER STRING"

    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    @StringDef(SOME_STRING, SOME_OTHER_STRING)
    annotation class StrAnno
}


