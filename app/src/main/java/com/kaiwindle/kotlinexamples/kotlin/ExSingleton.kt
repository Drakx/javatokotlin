/*
 * Kai Windle Copyright (c) 17/08/17 07:18/$month/$year
 * ExSingleton.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 *
 * https://medium.com/@adinugroho/singleton-in-kotlin-better-approach-8c5e28a140a5#.cofpberlr
 *
 * Mr. Andrey Breslav - Kotlin language designer
 */
object ExSingleton {
    init {
        println("This ($this) is a singleton")
    }

    var b: String? = null
}



