/*
 * Kai Windle Copyright (c) 17/08/17 10:47/$month/$year
 * ExChildClass.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExChildClass(val childname: String) : ExParentClass("Kai", "Windle")


