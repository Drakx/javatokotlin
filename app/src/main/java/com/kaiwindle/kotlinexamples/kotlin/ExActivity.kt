/*
 * Kai Windle Copyright (c) 17/08/17 07:31/$month/$year
 * ExActivity.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.kaiwindle.kotlinexamples.R
import com.kaiwindle.kotlinexamples.java.ExEnum
import com.kaiwindle.kotlinexamples.java.ExEnumStringAnnotation
import com.kaiwindle.kotlinexamples.java.ExEnumStringAnnotation.StrAnno
import com.kaiwindle.kotlinexamples.java.ExInterface
import kotlinx.android.synthetic.main.ex_activity_kotlin.*

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExActivity : AppCompatActivity() {
    @TargetApi(Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.ex_activity_kotlin)

        // Pre Kotlin 1.4
        val exCustomView = findViewById<View>(R.id.kotlin_custom_view) as ExCustomView

        // Kotlin 1.4+
        press_me_button.setOnClickListener { exCustomView.text = "Kotlin custom View" }

        val collections = ExCollections()
        @StrAnno val s: String = ExEnumStringAnnotation.SOME_OTHER_STRING
        Log.d("TAG", s)

        val modOne = ExModel("Kai", "Windle", 36)
        println("ModOne: $modOne")

        val modTwo = ExModelPartTwo()
        modTwo.firstname = "Jeff"
        println("ModTwo: ${modTwo.age}")

        val modThree = ExChildClass("Megan")
        println("ModThree: ${modThree.firstname} ${modThree.lastname} ${modThree.childname}")

        val exEnum = ExEnum.SECOND_NUM
        when (exEnum) {
            ExEnum.FIRST_NUM -> return
            ExEnum.SECOND_NUM -> return
            else -> {
            }
        }

        val exInterface = ExInterface { Log.d("Interface", "Implemented") }
        ExSingleton.b = "Test"


//        val collections = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            ExCollections()
//        } else {
//            Log.d("ExActivity", "Nothing to do")
//        }
    }
}


