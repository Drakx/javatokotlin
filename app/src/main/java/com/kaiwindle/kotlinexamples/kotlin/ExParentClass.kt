/*
 * Kai Windle Copyright (c) 17/08/17 10:46/$month/$year
 * ExExtendClass.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
open class ExParentClass(val firstname: String, val lastname: String)


