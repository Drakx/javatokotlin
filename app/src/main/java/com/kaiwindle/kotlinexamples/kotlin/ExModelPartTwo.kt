/*
 * Kai Windle Copyright (c) 17/08/17 10:40/$month/$year
 * ExModelPartTwo.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

class ExModelPartTwo {
    var firstname: String = "Test"
    set(value) {
        field = value
    }

    var lastname = "Lastname"
    var age: Int = 12
    set(value) {
        field = value
    }
}