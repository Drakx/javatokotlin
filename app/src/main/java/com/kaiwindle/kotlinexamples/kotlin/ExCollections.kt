/*
 * Kai Windle Copyright (c) 17/08/17 11:06/$month/$year
 * ExCollections.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

import android.os.Build
import android.support.annotation.RequiresApi
import android.util.Log
import java.util.*

@RequiresApi(Build.VERSION_CODES.N)
/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExCollections {
    private val TAG = ExCollections::class.java.simpleName

    private var names: List<String>? = null
    private var cities: Array<String>? = null
    private var infoMap: HashMap<String, Int>? = null

    init {
        setupList()
        setupArray()
        setupMap()
        names?.forEach { Log.d(TAG, "List: $it") }
//        for (s in names!!) {
//            Log.d(TAG, s)
//        }

        cities?.forEach { Log.d(TAG, "String[]: $it") }
//        for (i in cities!!.indices) {
//            Log.d(TAG, cities!![i])
//        }

        for ((key, value) in infoMap!!) {
            Log.d(TAG, "HashMap: $key : $value")
        }

    }

    private fun setupList() {
        names = object : ArrayList<String>() {
            init {
                add("Kai")
                add("Tom")
                add("Mike")
                add("Jeff")
            }
        }
    }

    private fun setupArray() {
        // Alternative way
        cities = arrayOf<String>("Manchester", "Leeds", "London")
    }

    private fun setupMap() {
        infoMap = HashMap()
        infoMap.let {
            it?.put("Kai", 1)
            it?.put("Amani", 1)
            it?.put("John", 1)
        }

//        infoMap?.put("Kai", 1)
//        infoMap?.put("John", 2)
//        infoMap?.put("Amani", 3)
    }
}