/*
 * Kai Windle Copyright (c) 17/08/17 07:18/$month/$year
 * ExInterface.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
interface ExInterface {
    fun foo()
}



