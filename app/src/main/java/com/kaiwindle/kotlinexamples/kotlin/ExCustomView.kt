/*
 * Kai Windle Copyright (c) 17/08/17 07:12/$month/$year
 * ExCustomView.kt
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.kotlin

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */
class ExCustomView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null,
                                             defStyleAttr: Int = 0) : TextView(context, attrs,
        defStyleAttr) {

    init {
        text = "Custom Text for CustomView"
    }
}

