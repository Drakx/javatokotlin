/*
 * Kai Windle Copyright (c) 17/08/17 07:18/$month/$year
 * MainActivity.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


/**
 * This project will show both Java and Kotlin
 * versions of common methods/classes/interfaces
 *
 * This is to serve only as a helper until such time
 * Kotlin has become second nature
 *
 * NOTE: This will sometimes display Lambda expressions
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button jButton = (Button) findViewById(R.id.btn_java);
        jButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                        com.kaiwindle.kotlinexamples.java.ExActivity.class));
            }
        });

        Button kButton = (Button) findViewById(R.id.btn_kotlin);
        kButton.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this,
                    com.kaiwindle.kotlinexamples.kotlin.ExActivity.class));
        });
    }
}


