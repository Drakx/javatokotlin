/*
 * Kai Windle Copyright (c) 17/08/17 07:17/$month/$year
 * ExEnumStringAnnotation.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 * https://developer.android.com/studio/write/annotations.html#enum-annotations
 */

public class ExEnumStringAnnotation {
    public final static String SOME_STRING = "SOME STRING";
    public final static String SOME_OTHER_STRING = "SOME OTHER STRING";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            SOME_STRING,
            SOME_OTHER_STRING
    })

    public @interface StrAnno {
    }
}
