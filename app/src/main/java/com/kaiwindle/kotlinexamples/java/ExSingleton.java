/*
 * Kai Windle Copyright (c) 17/08/17 07:19/$month/$year
 * ExSingleton.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExSingleton {
    public String b = null;

    private static ExSingleton instance;
    public static ExSingleton getInstance() {
        if (instance == null) {
            instance = new ExSingleton();
        }

        return instance;
    }
}


