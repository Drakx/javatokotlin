/*
 * Kai Windle Copyright (c) 17/08/17 10:48/$month/$year
 * ExParenClass.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExParenClass {
    private String firstname;
    private String lastname;

    public ExParenClass(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
