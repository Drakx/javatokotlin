/*
 * Kai Windle Copyright (c) 17/08/17 10:49/$month/$year
 * ExChildClass.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExChildClass extends ExParenClass {
    private String childname;

    public ExChildClass() {
        super("Kai", "Windle");
    }

    public String getChildname() {
        return childname;
    }

    public void setChildname(String childname) {
        this.childname = childname;
    }
}
