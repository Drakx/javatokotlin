/*
 * Kai Windle Copyright (c) 17/08/17 07:11
 * ExCustomView.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExCustomView extends TextView {
    public ExCustomView(Context context) {
        super(context);
    }

    public ExCustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ExCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ExCustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        setText("Custom Text for CustomView");
    }
}

