/*
 * Kai Windle Copyright (c) 17/08/17 10:55/$month/$year
 * ExArray.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExCollections {
    private static final String TAG = ExCollections.class.getSimpleName();

    private List<String> names;
    private String[] cities;
    private HashMap<String, Integer> infoMap;

    public ExCollections() {
        setupList();
        setupArray();
        setupMap();

        for (String s : names) {
            Log.d(TAG, s);
        }

        for (int i = 0; i < cities.length; i++) {
            Log.d(TAG, cities[i]);
        }

        for (Map.Entry<String, Integer> entry : infoMap.entrySet()) {
            Log.d(TAG, entry.getKey() + " : " + entry.getValue());
        }

    }

    private void setupList() {
        names = new ArrayList<String>() {
            {
                add("Kai");
                add("Tom");
                add("Mike");
                add("Jeff");
            }
        };
    }

    private void setupArray() {
        cities = new String[3];
        cities[0] = "Manchester";
        cities[1] = "Leeds";
        cities[2] = "Newcastle";

        // Alternative way
//        cities = new String[]{ "WakeField", "Leeds", "Newcastle" };
    }

    private void setupMap() {
        infoMap = new HashMap<>();
        infoMap.put("Kai", 1);
        infoMap.put("John", 2);
        infoMap.put("Amani", 3);
    }
}
