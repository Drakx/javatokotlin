/*
 * Kai Windle Copyright (c) 17/08/17 07:19/$month/$year
 * ExInterface.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public interface ExInterface {
    void foo();
}

