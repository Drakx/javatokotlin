/*
 * Kai Windle Copyright (c) 17/08/17 08:10/$month/$year
 * ExRecyclerViewAdapter.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExRecyclerViewAdapter extends RecyclerView.Adapter<ExRecyclerViewAdapter.ViewHolder> {
    ExModel model;

    public ExRecyclerViewAdapter(ExModel model) {
        this.model = model;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return model.getAge();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleText;

        public ViewHolder(View itemView) {
            super(itemView);

            titleText = new TextView(itemView.getContext());
        }
    }
}
