/*
 * Kai Windle Copyright (c) 17/08/17 07:21/$month/$year
 * ExActivity.java
 * KotlinExamples
 */

package com.kaiwindle.kotlinexamples.java;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.kaiwindle.kotlinexamples.R;

/**
 * Created by Kai on 17/08/2017.
 * KotlinExamples
 */

public class ExActivity extends AppCompatActivity {
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ex_activity);

        final ExCustomView exCustomView = (ExCustomView) findViewById(R.id.java_custom_view);
        Button button = (Button) findViewById(R.id.press_me_button);

        button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                exCustomView.setText("Custom Java View");
            }
        });

        ExCollections collections = new ExCollections();

        @ExEnumStringAnnotation.StrAnno String s;
        s = ExEnumStringAnnotation.SOME_OTHER_STRING;
        Log.d("TAG", s);

        ExEnum exEnum = ExEnum.SECOND_NUM;

        switch (exEnum) {
            case FIRST_NUM:
                return;

            case SECOND_NUM:
                return;
            default:
                break;
        }

        ExInterface exInterfaceLambda = () -> Log.d("Interface", "Implemented");

        ExInterface exInterface = new ExInterface() {
            @Override
            public void foo() {
               Log.d("Interface", "Implemented");
            }
        };

        ExSingleton.getInstance().b = "test";
    }
}


